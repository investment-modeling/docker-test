import express, {Request, Response} from "express";

const app = express();
const port = 5022;

app.get("/", (req: Request, res: Response) => {
  res.status(200).json({message: "Hello, World!"});
})

app.listen(port, () => {
  console.log(`Listening on http://localhost:${port}`)
})