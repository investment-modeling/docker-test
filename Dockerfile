FROM node:16.11

WORKDIR /app

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY . .

RUN npm run build

EXPOSE 5022

CMD ["npm", "run", "watch"]