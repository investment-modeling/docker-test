# Docker Test Project
A project that tests you have docker installed correctly.

## Steps to get this project running

### 1. Install Docker
Install docker desktop for your environment over here: https://www.docker.com/products/docker-desktop

### 2. Clone this repository
Make sure you have git installed, and then clone this repository. You can grab the repository clone command from bitbucket. Ideally, you should be using ssh keys with git.
`git clone git@bitbucket.org:investment-modeling/docker-test.git`

### 3. Build and run the container
With docker desktop running (make sure it says docker engine is running), open up a shell to the project directory and use the command `docker-compose up`
This should build and run the sample application. Once it is running it should be available on port 5022 in your web browser.

Once you have confirmed that the web server is running, in the project go to src/index.ts and edit the message in this snippet:
```ts
app.get("/", (req: Request, res: Response) => {
  res.status(200).json({message: "Hello, World!"});
})
```

Upon saving check that the project was reloaded inside the docker container, and then refresh the web browser to see if the message changed.

### 4. Tearing down the containers
After you've finished with the containers, you can bring them down by using the `docker-compose down` command.